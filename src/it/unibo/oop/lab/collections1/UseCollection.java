package it.unibo.oop.lab.collections1;
import java.util.*;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int START = 1000;
	private static final int STOP = 2000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> arrList = new ArrayList<Integer>();
        
        for(int i = START; i<STOP; i++) {
        	arrList.add(i);
        }
        
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        List<Integer> llist = new LinkedList<Integer>(arrList);

        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        int tmp = arrList.size()-1;
        arrList.set(arrList.size()-1, arrList.get(0));
        arrList.set(0, tmp);    
        
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        System.out.println("elements of arrlist: ");
        for(Integer i : arrList) {
        	System.out.println(i);
        }
        
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        long time = System.nanoTime();
        for (int i = 1; i <= 100000; i++) {
            arrList.add(0, i);
        }
        
        time = System.nanoTime() - time;
        System.out.println("time arrlist to inserting new elements in the head: " +time);     
        
        long time1 = System.nanoTime();
        for (int i = 1; i <= 100000; i++) {
            llist.add(0, i);
        }
  
        time1 = System.nanoTime() - time1;      
        System.out.println("time llist to inserting new elements in the head: " + time1);
        
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        long time2 = System.nanoTime();
        for (int i = 1; i <= 1000; i++) {
            arrList.get(arrList.size()/2);
        }
        
        time = System.nanoTime() - time2;
        System.out.println("time arrlist to read the middle position: " + time2);     
        
        long time3 = System.nanoTime();
        for (int i = 1; i <= 1000; i++) {
        	llist.get(llist.size()/2);
        }
  
        time3 = System.nanoTime() - time3;      
        System.out.println("time llist to read the middle position: " + time3);
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        final Map<String, Long> population = new HashMap<String,Long>();
        
        population.put("Africa" , 1_110_635_000L);
        population.put("Americas" , 972_005_000L);
        population.put("Antarctica" , 0L);
        population.put("Asia" , 4_298_723_000L);
        population.put("Europe", 742_452_000L);
        population.put("Oceania", 38_304_000L);
               
        /*
         * 8) Compute the population of the world 
         */
        System.out.println("total population in the world: " + (population.get("Africa") + population.get("Americas") + population.get("Antarctica") +
        			population.get("Asia") + population.get("Europe") + population.get("Oceania")));
    }
}
